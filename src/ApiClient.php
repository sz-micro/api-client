<?php

namespace GranitSDK\Service;

use GranitSDK\Logger;

class ApiClient  extends \Curl\Curl
{
	public function getResponse()
	{
		$response = parent::getResponse();

		if (!$this->isSuccess()) {
			Logger::get()->error('Api unreachable', [
				'calledUrl' => $this->getEndpoint(),
				'curlError' => $this->getErrorCode()
			]);

			throw new \Exception('Api unreachable');
		}

		$response = json_decode($response, true);
		if (!$response) {

			Logger::get()->error('Api unreachable', [
				'calledUrl' => $this->getEndpoint(),
				'rawResponse' => $response
			]);

			throw new \Exception('Api response not json');
		}

		if ($response['code'] != 200) {

			Logger::get()->error('Api response not 200', [
				'calledUrl' => $this->getEndpoint(),
				'jsonResponse' => $response
			]);

			throw new \Exception('Api response not 200');
		}

		return $response;
	}
}